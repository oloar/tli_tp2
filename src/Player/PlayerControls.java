package Player;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;

public class PlayerControls extends ButtonBar {
    private Button backward;
    private PlayButton play;
    private Button forward;

    public PlayerControls() {
        super();
        this.backward = new Button("\u25C3\u25C3");
        this.play = new PlayButton();
        this.forward= new Button("\u25B9\u25B9");
        this.getButtons().addAll(backward, play, forward);
    }

    public void setBackwardHandler(EventHandler<ActionEvent> event) {
        this.backward.setOnAction(event);
    }


    public void setForwardHandler(EventHandler<ActionEvent> event) {
        this.forward.setOnAction(event);
    }

    public PlayButton getPlayButton() {
        return this.play;
    }
}
