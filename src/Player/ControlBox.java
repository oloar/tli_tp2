package Player;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.VBox;


public class ControlBox extends VBox {
    private PlayerControls playerControls;
    private OrderControls orderControls;

    public ControlBox() {
        super();
        this.playerControls = new PlayerControls();
        this.orderControls = new OrderControls();
        this.getChildren().addAll(this.playerControls, this.orderControls);
    }

    public void setBackwardHandler(EventHandler<ActionEvent> event) {
        this.playerControls.setBackwardHandler(event);
    }


    public void setForwardHandler(EventHandler<ActionEvent> event) {
        this.playerControls.setForwardHandler(event);
    }

    public void setPreviousHandler(EventHandler<ActionEvent> event) {
        this.orderControls.setPreviousHandler(event);
    }
    public void setStopHandler(EventHandler<ActionEvent> event) {
        this.orderControls.setStopHandler(event);
    }
    public void setNextHandler(EventHandler<ActionEvent> event) {
        this.orderControls.setNextHandler(event);
    }

    public PlayButton getPlayButton() {
        return this.playerControls.getPlayButton();
    }

    public boolean isPlaying() {
        return this.playerControls.getPlayButton().isPlaying();
    }
}
