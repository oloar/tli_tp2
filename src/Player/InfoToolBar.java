package Player;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;


public class InfoToolBar extends BorderPane {
    private Slider volumeSlider;
    private HBox tools;
    private Button equalizer;
    private ToggleButton toggleListView;

    public InfoToolBar() {
        volumeSlider = new Slider(0,100,0);
        tools = new HBox();
        equalizer = new Button("|||");
        toggleListView = new ToggleButton(":=");

        equalizer.setOnAction(event -> {
            Alert popup = new Alert(Alert.AlertType.INFORMATION, "There should be lots of sliders and buttons here.");
            Button close = new Button("Close");
            close.setCancelButton(true);
            popup.setTitle("Equalizer");
            popup.setHeaderText("Equalizer");

            popup.showAndWait();
        });

        tools.getChildren().addAll(equalizer, toggleListView);
        this.setLeft(volumeSlider);
        this.setRight(tools);
    }

    public void setToggleListViewEventHandler(EventHandler<ActionEvent> event) {
        this.toggleListView.setOnAction(event);
    }

    public ToggleButton getToggleListView() {
        return toggleListView;
    }
}
