package Player;

import javafx.scene.control.Label;

public class TimeLabel extends Label {
    public TimeLabel() {
        super();
        this.setTime(0);
    }

    public void setTime(double time) {
        int m = (int) (time / 60);
        int s = (int) (time % 60);
        String timeStr = "";
        timeStr+=m + ":";
        if (s < 10)
            timeStr+="0";
        timeStr+=s;
        this.setText(timeStr);
    }
}
