package Player;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;


public class PlayButton extends Button {
    private Boolean isPlaying;

    public PlayButton() {
        super("\u25B6");
        this.isPlaying = false;
        this.setOnAction(event -> {
            if (this.isPlaying) {
                this.isPlaying = false;
                this.setText("\u25B6");
            } else {
                this.setText("\u25AE \u25AE");
                this.isPlaying = true;
            }
        });
    }

    public void stop() {
        this.isPlaying = true;
        this.fire();
    }

    public boolean isPlaying() {
        return isPlaying;
    }
}
