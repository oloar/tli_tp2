package Player;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;



public class OrderControls extends ButtonBar {
    private Button prev;
    private Button stop;
    private Button next;

    public OrderControls() {
        super();
        prev = new Button("|\u25c3");
        stop = new Button("\u25fc");
        next = new Button("\u25b9|");
        setHandlers();
        this.getButtons().addAll(prev,stop,next);
    }
    private void setHandlers() {
        this.prev.setOnAction(event -> System.out.println("prev"));
        this.stop.setOnAction(event -> System.out.println("stop"));
        this.next.setOnAction(event -> System.out.println("next"));
    }

    public void setPreviousHandler(EventHandler<ActionEvent> event) {
        this.prev.setOnAction(event);
    }
    public void setStopHandler(EventHandler<ActionEvent> event) {
        this.stop.setOnAction(event);
    }
    public void setNextHandler(EventHandler<ActionEvent> event) {
        this.next.setOnAction(event);
    }
}
