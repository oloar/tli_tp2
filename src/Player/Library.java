package Player;

import javafx.scene.control.*;

public class Library extends TreeTableView<TextField> {

    private TreeTableColumn<TextField, String> title;
    private TreeTableColumn<TextField, String> artist;
    private TreeTableColumn<TextField, String> duration;

    public Library() {
        super();
        this.title = new TreeTableColumn<>("Title");
        this.artist = new TreeTableColumn<>("Artist");
        this.duration = new TreeTableColumn<>("Duration");

        //noinspection unchecked
        this.getColumns().addAll(title, artist, duration);
    }

}
