package Player;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;


public class InfoBox extends BorderPane {
    private BorderPane LabelBox;
    private Label titleLabel;
    private TimeLabel timeLabel;
    private Slider timeSlider;
    private InfoToolBar infoTools;

    public InfoBox() {
        super();

        createLabels();
        timeSlider = new Slider(0, 180, 0);
        timeSlider.valueProperty().addListener((observable, oldValue, newValue) -> timeLabel.setTime(timeSlider.getValue()));
        this.infoTools = new InfoToolBar();

        this.setTop(LabelBox);
        this.setCenter(timeSlider);
        this.setBottom(infoTools);
    }

    private void createLabels() {
        LabelBox = new BorderPane();
        titleLabel = new Label("Something, Something");
        timeLabel = new TimeLabel();
        timeLabel.setLabelFor(timeSlider);
        LabelBox.setLeft(titleLabel);
        LabelBox.setRight(timeLabel);
    }

    public void updateCurrentSong(String title, int duration) {
        timeSlider.setValue(0);
        timeSlider.setMax(duration);
        titleLabel.setText(title);
        timeLabel.setTime(timeSlider.getValue());
    }

    public void setToggleEventHandler(EventHandler<ActionEvent> event) {
        this.infoTools.setToggleListViewEventHandler(event);
    }
    public ToggleButton getToggle() {
        return this.infoTools.getToggleListView();
    }

    public int getTime() {
        return (int) timeSlider.getValue();
    }
    public int getMaxTime() {
        return (int) timeSlider.getMax();
    }

    public void setTime(int time) {
        timeSlider.setValue(time);
    }

}
