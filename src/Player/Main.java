package Player;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.util.Timer;
import java.util.TimerTask;

public class Main extends Application {
    private int currentSongIdx = 0;
    private String [] songNames = {"Music 1","Music 2","Music 3"};
    private String [] artistNames = {"Artist 1","Artist 2","Artist 3"};
    private int [] durations = {123,432,295};


    @Override
    public void start(Stage primaryStage) {
        Timer timer = new Timer();
        BorderPane root = new BorderPane();

        Library lib = new Library();
        ControlBox controlBox = new ControlBox();
        InfoBox infoBox = new InfoBox();
        infoBox.updateCurrentSong(songNames[currentSongIdx]+" - "+artistNames[currentSongIdx], durations[currentSongIdx]);
        /* Handlers */
        controlBox.setBackwardHandler(event -> {
            int currentTime = infoBox.getTime();
            if (currentTime >= 10) {
                infoBox.setTime(currentTime - 10);
            } else if (currentTime > 0) {
                infoBox.setTime(0);
            }
        });

        controlBox.setForwardHandler(event -> {
            int currentTime = infoBox.getTime();
            int maxTime = infoBox.getMaxTime();

            if (currentTime <= maxTime - 10) {
                infoBox.setTime(currentTime + 10);
            } else if (currentTime < maxTime) {
                infoBox.setTime(maxTime);
            }
        });

        controlBox.setStopHandler(event -> {
            infoBox.setTime(0);
            controlBox.getPlayButton().stop();
        });

        infoBox.setToggleEventHandler(event -> {
            if (infoBox.getToggle().isSelected()) {
                root.setBottom(lib);
                primaryStage.setHeight(500);
                unlockHeight(primaryStage, Screen.getPrimary().getVisualBounds().getHeight());
            } else {
                root.setBottom(null);
                primaryStage.setHeight(100);
                lockHeight(primaryStage, 100);
            }
        });

        controlBox.setPreviousHandler(event -> {
            currentSongIdx = previousIndex();
            infoBox.updateCurrentSong(songNames[currentSongIdx]+" - "+artistNames[currentSongIdx],
                    durations[currentSongIdx]);
        });

        controlBox.setNextHandler(event -> {
            currentSongIdx = (currentSongIdx + 1) % 3;
            infoBox.updateCurrentSong(songNames[currentSongIdx]+" - "+artistNames[currentSongIdx],
                    durations[currentSongIdx]);
        });

        primaryStage.heightProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() < 200) {
                infoBox.getToggle().setSelected(false);
                infoBox.getToggle().fireEvent(new ActionEvent());
            }
        });

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    int currentTime;
                    if (controlBox.isPlaying()) {
                        currentTime = infoBox.getTime();
                        infoBox.setTime(currentTime + 1);
                    }
                });
            }
        }, 0, 1000);
        root.setLeft(controlBox);
        root.setCenter(infoBox);

        controlBox.setMaxHeight(60);
        infoBox.setMaxHeight(60);

        primaryStage.setOnCloseRequest(event -> {
            timer.cancel();
        });

        primaryStage.setTitle("VLC ++");
        primaryStage.setMinHeight(100);
        primaryStage.setMinWidth(600);

        primaryStage.setScene(new Scene(root));
        primaryStage.setHeight(100);
        primaryStage.setWidth(600);
        primaryStage.show();
    }

    private void lockHeight(Stage s, double h) {
        s.setMaxHeight(h);
        s.setMinHeight(h);
    }

    private void unlockHeight(Stage s, double h) {
        s.setMaxHeight(h);
        s.setMinHeight(100);
    }

    private int previousIndex() {
        int res = currentSongIdx - 1;
        return (res == -1)?2:res;

    }


    public static void main(String[] args) {
        launch(args);
    }
}
